//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;


var requestjson= require("request-json");

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/gceja/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;

var urlMovimientosMlab= "https://api.mlab.com/api/1/databases/gceja/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlMovimientosMlab);

var urlUsuariosMlab= "https://api.mlab.com/api/1/databases/gceja/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuariosMlab = requestjson.createClient(urlUsuariosMlab);


var path = require('path');

var bodyparser = require('body-parser')
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-with, Context-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movimientoJSON = require('./movimientosv2.json');

app.get('/', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/Clientes', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.send("Aquí estan los clientes devueltos nuevos");
});

app.get('/Clientes/:idcliente', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.send("Aquí tiene al cliente número: " + req.params.idcliente);
});

app.get('/Movimientos/v1', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.sendfile('movimientosv1.json');
});

app.get('/Movimientos/v2', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.send(movimientoJSON);
});

app.get('/Movimientos/v2/:index/:otroparametro', function(req, res) {
  console.log(req.params.index);
  console.log(req.params.otroparametro);
  //res.send('Hola Mundo nodejs');
  res.send(movimientoJSON[req.params.index-1]);
});

app.get('/Movimientosq/v2', function(req, res) {
  console.log(req.query);
  //res.send('Hola Mundo nodejs');
  res.send('Recibido');
});


app.post('/', function(req, res) {
  res.send('Hemos recibido su petición POST');

})

app.post('/Movimientos/v2', function(req, res) {
  var nuevo= req.body
  nuevo.id=movimientoJSON.length + 1;
  movimientoJSON.push(nuevo)
  res.send('Movimiento dado de alta');

})

app.get('/Usuarios', function (req, res) {
  usuariosMlab.get('', function (err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

app.get('/Movimientos', function (req, res) {
  clienteMlab.get('', function (err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

app.post('/Movimientos', function(req, res) {
  clienteMlab.post('', req.body, function (err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })

})

app.post('/Usuarios', function(req, res) {
  usuariosMlab.post('', req.body, function (err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })

})



app.post('/apitechu/v5/login', function(req, res) {
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query)

  clienteMlabRaiz.get('', function (err, resM, body) {
    if (!err) {
      if (body.length == 1){
        res.status(200).send('Usuario logado');
      }else{
        res.status(404).send('Usuario no encontrado');
      }

    }
  })

})
